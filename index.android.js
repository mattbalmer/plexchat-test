/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  BackAndroid,
  AppRegistry,
  View,
  Navigator
} from 'react-native';
import { PxList } from 'PlexchatTest/source/components/px-list';
import games from 'PlexchatTest/source/data/games.json';

let navigator = null;

class PlexchatTest extends Component {
  render() {
    BackAndroid.addEventListener('hardwareBackPress', () => {
      if (navigator && navigator.getCurrentRoutes().length > 1) {
        navigator.pop();
        return true;
      }
      return false;
    });

    return (
      <Navigator
        ref={(nav) => { navigator = nav; }}
        initialRoute={{ title: 'Games List', component: PxList, props: { games } }}
        renderScene={(route, navigator) => <route.component {...route.props} navigator={navigator} route={route} /> }
      />
    );
  }
}

AppRegistry.registerComponent('PlexchatTest', () => PlexchatTest);
