/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Navigator
} from 'react-native';
import { PxList } from 'PlexchatTest/source/components/px-list';
import games from 'PlexchatTest/source/data/games.json';

let navigator = null;

class PlexchatTest extends Component {
  render() {
    return (
      <Navigator
        ref={(nav) => { navigator = nav; }}
        initialRoute={{ title: 'Games List', component: PxList, props: { games } }}
        renderScene={(route, navigator) => <route.component {...route.props} navigator={navigator} route={route} /> }
      />
    );
  }
}

AppRegistry.registerComponent('PlexchatTest', () => PlexchatTest);
