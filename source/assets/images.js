const Assets = {
  'BoomBeach': require('image!boombeach'),
  'BoomBeach_thumb': require('image!boombeach_thumb'),
  'CoC': require('image!coc'),
  'CoC_thumb': require('image!coc_thumb'),
  'CoK': require('image!cok'),
  'CoK_thumb': require('image!cok_thumb'),
  'JurassicWorld': require('image!jurassicworld'),
  'JurassicWorld_thumb': require('image!jurassicworld_thumb'),
  'PuzzlesAndDragons': require('image!puzzlesanddragons'),
  'PuzzlesAndDragons_thumb': require('image!puzzlesanddragons_thumb')
};

export default Assets;