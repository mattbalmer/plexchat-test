import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight
} from 'react-native';

export class PxButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      styles: StyleSheet.create({
        button: Object.assign({}, styles.button, (props.styles ? props.styles.button || {} : {})),
        text: Object.assign({}, styles.text, (props.styles ? props.styles.text || {} : {}))
      })
    };
  }

  render() {
    return (
      <TouchableHighlight onPress={this.onPress.bind(this)}>
        <View style={this.state.styles.button}>
          <Text style={this.state.styles.text}>
            {this.props.text}
          </Text>
        </View>
      </TouchableHighlight>
    );
  }

  onPress() {
    if(this.props.onPress) {
      this.props.onPress();
    }
  }
}

const styles = {
  button: {
    padding: 8,
    paddingLeft: 16,
    paddingRight: 16,
    borderRadius: 3
  },
  text: {
    color: '#fff'
  }
};