import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableHighlight
} from 'react-native';
import Assets from 'PlexchatTest/source/assets/images';

export class PxListItem extends Component {
  render() {
    return (
      <TouchableHighlight onPress={this.onPress.bind(this)}>
        <View style={styles.row}>
          <Image source={Assets[this.props.game.thumbnail]} style={styles.thumb} />
          <Text>
            {this.props.game.name}
          </Text>
        </View>
      </TouchableHighlight>
    );
  }

  onPress() {
    this.props.onSelect(this.props.game);
  }
}

const styles = StyleSheet.create({
  thumb: {
    width: 50,
    height: 50,
    margin: 10
  },
  row: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#F5FCFF',
    borderBottomWidth: 1,
    borderBottomColor: '#eee'
  }
});