import React, { Component } from 'react';
import {
  Text,
  View,
  ListView
} from 'react-native';
import { PxListItem } from 'PlexchatTest/source/components/px-list-item';
import { PxGameDetailsScene } from 'PlexchatTest/source/scenes/px-game-details-scene';

export class PxList extends Component {
  constructor(props) {
    super(props);

    let ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    this.state = {
      dataSource: ds.cloneWithRows(this.props.games)
    };
  }

  render() {
    return (
      <ListView
        dataSource={this.state.dataSource}
        renderRow={rowData => <PxListItem game={rowData} onSelect={this.onSelect.bind(this)} /> }
      />
    );
  }

  onSelect(game) {
    this.props.navigator.push({
      component: PxGameDetailsScene,
      props: {
        game
      }
    });
  }
}