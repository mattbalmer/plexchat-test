import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  ListView,
  TouchableHighlight,
  TextInput,
  ScrollView
} from 'react-native';
import { PxButton } from 'PlexchatTest/source/components/px-button';
import Assets from 'PlexchatTest/source/assets/images';

export class PxGameDetailsScene extends Component {
  constructor(props) {
    super(props);

    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    this.state = {
      comments: this.props.game.comments,
      dataSource: this.ds.cloneWithRows(this.props.game.comments)
    };
  }
  render() {
    return (
      <View style={styles.details}>

        <View style={styles.header}>
          <Image source={Assets[this.props.game.thumbnail]} style={styles.thumb} />
          <Text>
            {this.props.game.name}
          </Text>
        </View>

        <ScrollView>
          <View style={styles.captionContainer}>
            <Text style={styles.caption}>
              {this.props.game.caption}
            </Text>
          </View>

          <ListView
            dataSource={this.state.dataSource}
            renderRow={rowData => {
              return (
                <View style={styles.commentContainer}>
                  <Text>{rowData}</Text>
                </View>
              )
            }}
          />

          <View style={styles.leaveCommentContainer}>
            <View>
              <Text>Leave a comment:</Text>
            </View>
            <View style={{
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}>
              <TextInput
                style={{height: 40, borderColor: 'gray', borderWidth: 1, flex: 1}}
                onChangeText={(commentText) => this.setState({ commentText })}
                value={this.state.commentText}
                multiline={true}
                lines={2}
              />
              <PxButton text="Comment" styles={{ button: { backgroundColor: '#3c3' } }} onPress={this.submit.bind(this)} />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }

  submit() {
    let comments = this.state.comments.concat([this.state.commentText]);

    this.setState({
      comments,
      dataSource: this.ds.cloneWithRows(comments),
      commentText: ''
    });
  }
}

const styles = StyleSheet.create({
  details: {
    backgroundColor: '#F5FCFF',
    flex: 1
  },
  thumb: {
    width: 50,
    height: 50,
    margin: 10
  },
  header: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#F5FCFF',
    borderBottomWidth: 1,
    borderBottomColor: '#eee',
    padding: 0,
    height: 70
  },
  captionContainer: {
    backgroundColor: '#e4e4e4'
  },
  caption: {
    margin: 10,
    fontStyle: 'italic'
  },
  commentContainer: {
    padding: 10,
    borderTopWidth: 1,
    borderTopColor: '#eee'
  },
  leaveCommentContainer: {
    padding: 10,
    backgroundColor: '#e4e4e4',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: -1
    },
    shadowOpacity: 0.7,
    shadowRadius: 3
  }
});
